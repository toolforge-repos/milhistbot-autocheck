# NAME

AutoCheck - 

# USAGE

AutoCheck i[OPTIONS]+ <article>
Assess article and update the MilHist template on the talk page.

Options:
  -d, --debug                debugging
  -f, --force                update page
  --ff, -u, --update     	 force update page
  -h, -?, --help             display help
  -n, --max=VALUE            number of pages to process
  -v, --verbose              vebosity


# DESCRIPTION

This requires a configuration file and Dotnet version 7 or 8.  

