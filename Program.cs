using System;
using System.Collections.Generic;
using NDesk.Options;

namespace Wikimedia
{
	public class AutoCheck
	{
		private int unchanged;
		private int upgrade;
		private int newgrade;
		private int downgrade;

		private int max = 1;
		private bool debugs = false;
		private bool force = false;
		private bool help = false;
		private bool update = false;
		private bool verbose = false;
		protected static readonly Dictionary<string, int> classes;

		private bool compareClass(string oldClass, string botClass)
		{
			if (oldClass.Equals("unclassified"))
			{
				newgrade++;
			}
			else if (!classes.ContainsKey(oldClass))
			{
				if (update)
				{
					newgrade++;
				}
				else
				{
					if (!oldClass.Equals(""))
					{
						throw new ApplicationException("unknown class: '" + oldClass + "'");
					}
				}
			}
			else
			{
				int diff = classes[botClass] - classes[oldClass];
				if (0 > diff)
				{
					downgrade++;
				}
				else if (0 == diff)
				{
					unchanged++;
					return false;
				}
				else if (0 < diff)
				{
					upgrade++;
				}
			}
			return true;
		}

		private void updateAllProjects(TalkPage talk, string botClass)
		{
			var milhist = talk.MilHist.ProjectTemplate;
			ProjectBannerShell? bannerShell = ProjectBannerShell.GetProjectBannerShell(talk);
			if (null == bannerShell)
			{
				milhist.Class = botClass;
				if (debugs || verbose)
				{
					Console.WriteLine("\tModified milhist template: " + milhist.Text);
				}
			}
			else
			{
				if (botClass.Equals("Redirect") || botClass.Equals("Disambig"))
				{
					milhist.Clear();
					milhist.Class = botClass;
				}
				else if (bannerShell.Class.Equals("List"))
				{
					milhist.IsList = true;
				}
				else if (milhist.IsList)
				{
					bannerShell.Class = "List";
				}
				else
				{
                    bannerShell.Class = botClass;
                }

                if (debugs || verbose)
				{
					Console.WriteLine("\tModified banner shell: " + bannerShell.Text);
				}
			}
		}

		private void autoCheck(ArticlePage article, TalkPage talk)
		{
			try
			{
				Cred.Instance.Showtime(article.Title);
				article.Load();
				try
				{
					talk.Load();
				}
				catch (PageMissingException)
				{
					if (force || update)
					{
						Cred.Instance.Showtime(talk.Title + " does not exist - creating it");
					}
				}

				var milhist  = talk.MilHist.ProjectTemplate;
				var oldClass = milhist.Class;
				var missing  = milhist.Missing();
				var rating   = milhist.Rating;
				var botClass = rating.Class;

				if (botClass.Equals("List") || botClass.Equals("CL") || botClass.Equals("BL"))
				{
					milhist.IsList = true;
				}

				if (debugs || verbose)
				{
					Console.WriteLine("\tOriginal: " + milhist.Text);
				}

				if (force || update)
				{
					milhist.Clear();
					milhist.Rating = rating;
				}

				updateAllProjects(talk, botClass);

				if (debugs || verbose)
				{
					Console.WriteLine("\told rating = " + oldClass);
					Console.WriteLine("\tprediction = " + article.Prediction());
					Console.WriteLine("\tbot rating = " + botClass);
					Console.WriteLine("\tModified: " + milhist.Text);
				}

				var changed = compareClass(oldClass, botClass);
				if (((changed || missing) && force) || update)
				{
					talk.Save("Automatic MILHIST checklist assessment - " + botClass + " class");
					if (changed)
					{
						Cred.Instance.Showtime("\tChanged from " + oldClass + " to " + botClass + " class");
					}
					else if (missing)
					{
						Cred.Instance.Showtime("\tAdded missing checklist items");
					}
					else
					{
						Cred.Instance.Showtime("\tUpdated to " + botClass + " class");
					}
				}
			}
			catch (ApplicationException ex)
			{
				string message = "Error in " + article.Title + ": " + ex.Message;
				Cred.Instance.Showtime(message);
			}
			catch (Exception ex)
			{
				string message = "Error in " + article.Title + ":\n" + ex.Message + "\n" + ex.StackTrace;
				if (debugs)
				{
					Debug.WriteLine(message);
				}
				else
				{
					Cred.Instance.Warning(message);
					Cred.Instance.Close();
				}
				Environment.Exit(1);
			}
		}

		private void autoCheck(List<Page> pages)
		{
			foreach (var page in pages)
			{
				// Could be a subcategory
				if (page is TalkPage && page is not TemplateTalkPage)
				{
					TalkPage? talk = page as TalkPage;
					if (null != talk)
					{
						autoCheck(talk.Article, talk);
					}
				}
			}
			Cred.Instance.Showtime(String.Format("{0} articles newly rated, {1} downgraded, {2} upgraded, {3} unchanged - total {4}",
								newgrade, downgrade, upgrade, unchanged, newgrade + downgrade + upgrade + unchanged));
		}

		private static void showHelp(OptionSet options)
		{
			Console.WriteLine("Usage: mono AutoCheck [OPTIONS]+ <article>");
			Console.WriteLine("Assess article and update the MilHist template on the talk page.");
			Console.WriteLine();
			Console.WriteLine("Options:");
			options.WriteOptionDescriptions(Console.Out);
			Environment.Exit(0);
		}

		List<string> options(string[] args)
		{
			var optionSet = new OptionSet() {
			{ "d|debug",     "debugging",    v => debugs  = v != null },
			{ "f|force",     "update page",  v => force   = v != null },
			{ "ff|u|update", "force update page",  v => update = v != null },
			{ "h|?|help",    "display help", v => help    = v != null },
			{ "n|max=",      "number of pages to process",  v => int.TryParse (v, out max) },
			{ "v|verbose",   "vebosity",     v => verbose = v != null },
		};

			List<string> extras = optionSet.Parse(args);

			if (help)
			{
				showHelp(optionSet);
			}

			return extras;
		}

		static AutoCheck()
		{
			classes = new Dictionary<string, int>()
			{
				{ "Project",  0 },
				{ "Redirect", 0 },
				{ "Disambig", 0 },
				{ "SIA",	  0 },
				{ "Stub",     1 },
				{ "Start",    2 },
				{ "List",     2 },
				{ "C",        3 },
				{ "CL",       3 },
				{ "B",        4 },
				{ "BL",       4 },
			};
		}

		private AutoCheck(string[] args)
		{
			Bot bot = Bot.Instance;
			var articles = options(args);
			Debug.On = debugs;
			Debug.Offline = false;

			Cred.Instance.Showtime("started");
			if (articles.Count > 0)
			{
				var articlePage = new ArticlePage(articles[0]);
				autoCheck(articlePage, articlePage.Talk);
			}
			else
			{
				var categoryList = new List<string>
				{
					"Military history articles with missing B-Class checklists",
					"Military history articles with incomplete B-Class checklists",
					"Unassessed military history articles",
					"Military history lists incorrectly assessed as articles",
				};

                var talkPages = new List<Page>();
				foreach (var category in categoryList)
				{
					if (talkPages.Count < max)
					{ 
						var query = new Query(category, max - talkPages.Count);
						talkPages.AddRange(bot.Category(query));
					}
				}

                autoCheck(talkPages);
			}
			Cred.Instance.Showtime("done");
		}

		static public void Main(string[] args)
		{
			new AutoCheck(args);
		}
	}
}
